Source: hdrhistogram
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 debhelper (>= 11),
 default-jdk,
 junit4,
 libmaven-bundle-plugin-java,
 libmaven-dependency-plugin-java,
 libreplacer-java,
 maven-debian-helper (>= 2.1)
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/java-team/hdrhistogram.git
Vcs-Browser: https://salsa.debian.org/java-team/hdrhistogram
Homepage: http://hdrhistogram.github.io/HdrHistogram/

Package: libhdrhistogram-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: High Dynamic Range (HDR) Histogram
 HdrHistogram is designed for recording histograms of value measurements in
 latency and performance sensitive applications. It supports the recording
 and analyzing of sampled data value counts across a configurable integer
 value range with configurable value precision within the range. Value
 precision is expressed as the number of significant digits in the value
 recording, and provides control over value quantization behavior across
 the value range and the subsequent value resolution at any given level.
